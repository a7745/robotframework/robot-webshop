*** Settings ***
Documentation       Tests for valid login.

Resource            ../../../Resources/Settings.resource

Test Setup          Initialize Tests
Test Teardown       Close Browser

*** Variables ***
${BROWSER}      chrome
${PASSWORD}     crypt:lbReCK5doEE0rOyRGkBBfOR/qybxZobb77lyJk0zRQUAhKmwOp6lI6+T/22tzFjmBlVPVZZE4hj/gYpx

*** Test Cases ***
Valid Login
    [Documentation]    Tests if user can login with correct login credentials.
    Enter Login Email    r.matz@test.com
    Enter Password    ${PASSWORD}
    Click Login Button
    Verify Successful Login
    Logout

*** Keywords ***
Initialize Tests
    Open Homepage    ${BROWSER}
    Click Login Link
