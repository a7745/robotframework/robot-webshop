FROM python:3.10-slim

WORKDIR /opt/robotframework

COPY requirements.txt requirements.txt

RUN python3 -m pip install --no-cache-dir -r \
    requirements.txt
